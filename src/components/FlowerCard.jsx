
import React from "react";
import { Card } from "semantic-ui-react";
import { Image } from "semantic-ui-react";
import { Icon, Button, Rating } from "semantic-ui-react";

const FlowerCard = (flower) => {
  const {title, image, text, price, addToCart, addedCount} = flower
  return (
    <Card>
    <Image src={image} wrapped ui={false} />
    <Card.Content>
      <Card.Header>{title}</Card.Header>
      <Card.Description>
        {text}
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
        <Icon name='rub' />
        {price}
    </Card.Content>
    <Button  onClick={addToCart.bind(this, flower)}>
      Добавить в корзину {addedCount > 0 && `(${addedCount})`}</Button>
  </Card>
  )
}
export default FlowerCard