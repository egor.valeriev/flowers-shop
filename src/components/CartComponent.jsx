
import React from "react"
import { Button, Image, List } from "semantic-ui-react"

const CartComponent = ({title, id, image, removeFromCart}) => (
    <List List selection divided verticalAlign='middle'>
    <List.Item>
      <List.Content floated='right'>
        <Button onClick={removeFromCart} color="red">Удалить</Button>
      </List.Content>
      <Image avatar src={image} />
      <List.Content>{title}</List.Content>
    </List.Item>
  </List>
)

export default CartComponent