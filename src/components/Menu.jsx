import React from "react";
import { Menu, Popup } from "semantic-ui-react";
import { Button, Image, List } from "semantic-ui-react"
import './Menu.css';


const CartComponent = ({title, image, id, removeFromCart}) => (
  <List selection divided verticalAlign='middle'>
  <List.Item>
      <Image avatar src={image} />
      <List.Content>{title}{` (${1})`}</List.Content>
    <List.Content floated='right'>
      <Button onClick={removeFromCart.bind(this, id)} color="red">Удалить</Button>
    </List.Content>
    
  </List.Item>
</List>
);


const TopMenu = ({totalPrice, count, items, addedCount}) => (
    <Menu>
        <Menu.Item
          name='browse'
        >
          Магазин цветов
        </Menu.Item>
        <Menu.Menu  position='right'
        >
          <Menu.Item 
            name='signup'
          >
            Итого: &nbsp; <b>{totalPrice}</b> &nbsp; руб.
          </Menu.Item>


          <Popup className="popup" trigger = {
            <Menu.Item
            name='help'
          >
            Корзина &nbsp; (<b>{count}</b>)
          </Menu.Item>
          }
          content={items.map(flower => <CartComponent {...flower} count={addedCount}/>)}
          on='click'
          hideOnScroll
          />
        </Menu.Menu>
      </Menu>
)

export default TopMenu