import React from "react"
import './Filter'
import { Input, Menu } from 'semantic-ui-react'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import * as filterActions from '../../actions/filter'

const Filter = (props) => {
    const { setFilter, filterBy, searchQuery, setSearchQuery } = props
    return (
        <Menu secondary>
        <Menu.Item
          active={filterBy === 'all'}
          onClick={setFilter.bind(this, 'all' )}
        >Все</Menu.Item>
        <Menu.Item
          active={filterBy === 'price_high'}
          onClick={setFilter.bind(this, 'price_high' )}
        >Дорогие</Menu.Item>
        <Menu.Item
          active={filterBy === 'price_low'}
          onClick={setFilter.bind(this, 'price_low' )}
        >Дешёвые</Menu.Item>
        <Menu.Menu position='right'>
          <Menu.Item>
            <Input icon='search' placeholder='Search...'
            onChange={e => setSearchQuery(e.target.value)}
              value={searchQuery}
            />
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    )
}
const mapStateToProps = ({filter}) => ({
    filterBy: filter.filterBy,
    searchQuery: filter.searchQuery
  })

  const MapDisptachToProps = dispatch =>
   ({
     ...bindActionCreators(filterActions, dispatch),
   })

export default connect(mapStateToProps, MapDisptachToProps)(Filter)