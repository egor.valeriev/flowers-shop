import { connect } from "react-redux";
import { bindActionCreators } from "@reduxjs/toolkit";
import * as cartActions from '../actions/cart';
import FlowerCard from '../components/FlowerCard'

const mapStateToProps = ({cart}, {id}) => (
    {
      addedCount: cart.items.reduce(
          (count, flower) => count + (flower.id === id ? 1 : 0 ), 0,),  
  })
  const MapDisptachToProps = dispatch =>
   ({
     ...bindActionCreators(cartActions, dispatch),
   })

export default connect(mapStateToProps, MapDisptachToProps)(FlowerCard)