import { connect } from "react-redux";
import { bindActionCreators } from "@reduxjs/toolkit";
import * as cartActions from '../actions/cart';
import TopMenu from '../components/Menu'
import uniqBy from 'lodash/unionBy'

const mapStateToProps = ({cart}) => (
    {
    totalPrice: cart.items.reduce((total, flower) => total + flower.price, 0),
    count: cart.items.length,
    items: uniqBy(cart.items, o => o.id),
  })
  const MapDisptachToProps = dispatch =>
   ({
     ...bindActionCreators(cartActions, dispatch),
   })

export default connect(mapStateToProps, MapDisptachToProps)(TopMenu)