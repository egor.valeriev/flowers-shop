import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import { BrowserRouter} from 'react-router-dom';
import createStore from './store';
import { Provider } from 'react-redux';
import 'semantic-ui-css/semantic.min.css'
import './index.css';


const store = createStore();


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <App/>
      </BrowserRouter> 
    </Provider>  
  </React.StrictMode>,
  document.getElementById('root')
);

