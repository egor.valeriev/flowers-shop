import { combineReducers } from "redux";

import flowers from './flowers'
import cart from "./cart";
import filter from "./filter";

export default combineReducers({
    flowers,
    filter,
    cart
})