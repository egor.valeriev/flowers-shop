export const data = {
  "flowers": [
    {
      "id": 1,
      "image": "https://kaliningrad.rus-buket.ru/files/icons/icon-3157_0.png",
      "title": "Красивый букет",
      "text": "Очень красивый букет, Вы будете в восторге!!!",
      "price": 4758
    },
    {
      "id": 2,
      "image": "https://kaliningrad.rus-buket.ru/files/icons/icon-5307.png",
      "title": "Букет (Притяжение)",
      "text": "Можно купить, а можно подарить себе за деньги",
      "price": 1265
    },
    {
      "id": 3,
      "image": "https://kaliningrad.rus-buket.ru/files/icons/icon-1366.png",
      "title": "Букет Рубиновое Сердце",
      "text": "Розы Розы Розы, красота <3",
      "price": 1001
    },
    {
      "title": "Прекрасный Букет",
      "image": "https://kaliningrad.rus-buket.ru/files/icons/icon-401.png",
      "text": "Специально для любимой",
      "price": 2889,
      "id": 4
    },
    {
      "title": "Роза",
      "image": "https://kaliningrad.rus-buket.ru/files/icons/icon-5320.png",
      "text": "Розовые розы Светки Соколовой",
      "price": 4665,
      "id": 5
    },
    {
      "title": "Калининград",
      "image": "https://kaliningrad.rus-buket.ru/files/icons/icon-5401.png",
      "text": "По существу и строго",
      "price": 1587,
      "id": 8
    }
  ]
}