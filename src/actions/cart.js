export const addToCart = obj => ({
    type: "ADD_FLOWER_TO_CART",
    payload: obj
})

export const removeFromCart = id => ({
    type:'REMOVE_FLOWER_FROM_CART',
    payload: id
})
