export const setFlowers = (flowers) => ({
    type: "SET_FLOWERS",
    payload: flowers
})