import './App.css';
import React, { useEffect} from 'react';
import './store'
import { connect } from 'react-redux'
import * as flowersActions from './actions/flowers'
import * as filterActions from './actions/filter';
import host from './helpers/constants'
import getData from './helpers/api'
import TopMenu from './containers/Menu';
import { Card, Container } from 'semantic-ui-react'
import FlowerCard from './containers/FlowerCard';
import Filter from './components/Filter/Filter';
import { bindActionCreators } from 'redux'
import { orderBy } from 'lodash';
import { data } from './db';

function App(props) {
  console.log();
  const flowers = props.flowers
  const setFlowers = props.setFlowers
  const isReady = props.isReady
// оригинальный код, где запрос идёт на JSONсервер localhost с портом 3004 (файл db.json)  
//   useEffect(()=> {
//        getData(`${host}/flowers`).then (response => {
//        setFlowers(response)     
//     })
// }, []) 

useEffect(()=> {
  setFlowers(data.flowers)
}, []) 
    return (
    <Container>
      <TopMenu/>
      <Filter/>
      <Card.Group itemsPerRow={4}>
            {!isReady ? <div>Загрузка...</div> :
            flowers.map(flower => (          
                <FlowerCard key={flower.id}
                  title={flower.title}
                  image={flower.image}
                  price={flower.price}
                  text={flower.text}
                  id={flower.id}
                />
              ))
          }
      </Card.Group>
    </Container>
  )

}
const sortBy = (flowers, filterBy) => {
  switch (filterBy) {
    case 'all':
      return flowers;    
    case 'price_high':
      return orderBy(flowers, 'price', 'desc');
    case 'price_low':
      return orderBy(flowers, 'price', 'asc');   
    case 'popular':
    return flowers;
    default:
  }
}
const filterFlowers = (flowers, searchQuery) => {
  
  return (
  flowers = flowers.filter(o => o.title.toLowerCase().indexOf(searchQuery.toLowerCase()) >= 0))}

const searchFlowers = (flowers, searchQuery, filterBy) => {
  return sortBy(filterFlowers(flowers, searchQuery), filterBy)
}

const mapStateToProps = ({flowers, filter}) => ({
  flowers: flowers.items && searchFlowers(flowers.items, filter.searchQuery, filter.filterBy),
  searchQuery: filter.searchQuery,
  isReady: flowers.isReady,
})

const MapDisptachToProps = dispatch =>
 ({
   ...bindActionCreators(flowersActions, dispatch),
   ...bindActionCreators(filterActions, dispatch),
 })
export default connect(mapStateToProps, MapDisptachToProps)(App)
